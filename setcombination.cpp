#include "setcombination.h"
#include "ui_setcombination.h"
#include <windows.h>
#include <QSettings>
#include "menu.h"
#include "hotkeycontroll.h"
#include "windows.h"

SetCombination::SetCombination(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SetCombination)
{
    ui->setupUi(this);
    this->setWindowTitle("Нажмите комбинацию клавиш");
}

SetCombination::~SetCombination()
{
    delete ui;
}

void SetCombination::on_keySequenceEdit_keySequenceChanged(const QKeySequence &keySequence)
{
    QSettings settings("NikAndPasha", "desktopassistant");
    settings.setValue("keys", keySequence[0]);
    settings.setValue("keysS", keySequence.toString().split(",").first());
    emit changeKeys();
    this->close();
}
