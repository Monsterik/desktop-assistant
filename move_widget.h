#ifndef MOVE_WIDGET_H
#define MOVE_WIDGET_H
#include <QCoreApplication>
#include <QWidget>
#include <QLabel>
#include "chatbot.h"
#include "botanswer.h"

namespace Ui {
class Move_Widget;
}
/*!
    \class Move_Widget
    \brief Move_Widget - класс, наследующийся от QLabel, но при этом
        дополняющий его возможностью перемещения мышью


    Экземпляр класса создается в конструкторе MainWindow и задаётся
    как центральный виджет.

*/
class Move_Widget:public QLabel{
    Q_OBJECT
private:
    /*!
     * \brief Слот клика мыши для получения координат мыши.
     * \param event
     */
    void mousePressEvent(QMouseEvent *event);

    /*!
     * \brief Слот движения мыши. Осуществляет перемещение главного окна перетаскиванием за изображение.
     * \param event
     */
    void mouseMoveEvent(QMouseEvent *event);

    /*!
     * \brief Слот двойного клика. Создает экземпляр класса Menu и отображает его.
     * \param event
     */
    void mouseDoubleClickEvent(QMouseEvent *event);
public:
    QPoint mpos;
    ChatBot *chatbot;
    bool chatShowed = false;
    explicit Move_Widget(QWidget *parent = nullptr);
    void openWithHello();
    BotAnswer *botAnswer;
    void openCommand();

private slots:
    /*!
     * \brief Слот отправляет сигнал о смене главного изображения updateMainImage().
     */
    void sendToMainWindow();

    /*!
     * \brief Слот отправляет сигнал о переходе в трей inTrayMode().
     */
    void inTrayModeToMainWindow();

    /*!
     * \brief Слот отправляет сигнал о смене режима отображения главного окна onTopModeChanged().
     */
    void onTopModeChangedToMainWindow();

    /*!
     * \brief Слот отправляет сигнал о изменении списка событий eventsDataChanged().
     */
    void sendReloadEventsQuery();

    void newKeysRegister();


signals:
    /*!
     * \brief Сигнал о изменении главного изображения, отправлется из функции sendToMainWindow().
     */
    void updateMainImage();

    /*!
     * \brief Сигнал о переходе в трей, отправлется из функции inTrayModeToMainWindow().
     */
    void inTrayMode();

    /*!
     * \brief Сигнал о смене режима отображения главного окна, отправлется из функции
     *        onTopModeChangedToMainWindow().
     */
    void onTopModeChanged();

    /*!
     * \brief Сигнал об изменении сочетания клавиш.
     */
    void RegisterKeys();

    /*!
     * \brief Сигнал об изменении данных о событиях, отправлется из функции sendReloadEventsQuery().
     */
    void eventsDataChanged();

    void micInput();

    void showAnswer();
};


#endif // MOVE_WIDGET_H
