#ifndef EVENTSWINDOW_H
#define EVENTSWINDOW_H

#include <QDialog>

/*!
    \namespace Ui

    \brief Реализует связь класса с соответствующим ему ui. Особенность работы Qt.
*/
namespace Ui {
class EventsWindow;
}


/*!
    \class EventsWindow
    \brief EventsWindow класс - окно с принципом работы, как
        у диалогового окна. В данном окне проходит процесс работы
        с событиями.


    Класс реализует работу с событиями. Отображает все запланированные события,
    релизует удаление и редактирование событий. Реализует переход к окну добавления
    нового события.

*/
class EventsWindow : public QDialog
{
    Q_OBJECT

public:
    /*!
      \fn ComandsWindow::ComandsWindow(QWidget *parent)

      Конструктор. Задаёт заголовок окна. Задаёт флаги окна. Инициализирует
      параметры таблицы в ui.
      Загружает событиея из файла в таблицу в ui с помощью loadEventsToList().

      \sa loadEventsToList()

    */
    explicit EventsWindow(QWidget *parent = nullptr);

    /*!
      \brief Деструктор класса EventsWindow.

      Очищает выделенную в ходе работы память при удалении экземпляра.

    */
    ~EventsWindow();

signals:
    /*!
     * \brief Сигнал отправляется при добавлении, изменении или удалении события
     *      из списка.
     */
    void eventsDataChanged();

private slots:
    /*!
     * \brief Слот клика по кнопке "Добавить". Создается экземпляр класса AddEventWindow B
     *        отображается окно.
     */
    void on_btn_add_event_clicked();

    /*!
     * \brief Слот обновления таблицы событий в виджете. Срабатывает при получении сигнала из
     *          окна AddEventWindow при добавлении нового события и обновляет данные таблицы через
     *          считывание изменённого файла.
     */
    void updateTable();

    /*!
     * \brief Слот клика по кнопке "Сохранить". Сохраняет изменения таблицы перезаписью файла.
     */
    void on_btn_edit_event_clicked();

    /*!
     * \brief Слот клика по кнопке "Удалить". Удаляет событие из таблицы и перезаписывает в файл
     *        данные уже без удаляемого события.
     */
    void on_btn_remove_event_clicked();

private:
    Ui::EventsWindow *ui;

    /*!
     * \brief Функция загрузки данных из файла в таблицу.
     */
    void loadEventsToList();

};

#endif // EVENTSWINDOW_H
