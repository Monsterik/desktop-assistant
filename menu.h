#ifndef MENU_H
#define MENU_H

#include <QDialog>


/*!
    \namespace Ui

    \brief Реализует связь класса с соответствующим ему ui. Особенность работы Qt.
*/
namespace Ui {
class Menu;
}


/*!
    \class Menu
    \brief Menu класс - окно с принципом работы, как
        у диалогового окна. Данное окно используются для отображения
        пунктов меню, которые обеспечивают переход в соответствующие
        окна.

*/
class Menu : public QDialog
{
    Q_OBJECT

public:
    /*!
      \fn ComandsWindow::ComandsWindow(QWidget *parent)

      Конструктор. Задаёт заголовок окна. Задаёт флаги окна. Инициализирует значение
      кнопки отображения поверх всех окон через настройки приложения (QSettings).

    */
    explicit Menu(QWidget *parent = nullptr);

    /*!
     * \brief Устанавливает новое сочетание в найстройках приложения, оправляет сигнал
     *        на регистрацию сочетания.
     */
    void changeKeys();

    /*!
      \brief Деструктор класса Menu.

      Очищает выделенную в ходе работы память при удалении экземпляра.

    */
    ~Menu();

private slots:
    /*!
     * \brief Слот клика по кнопке "Завершить работу". Завершает работу приложения.
     */
    void on_btn_exit_clicked();

    /*!
     * \brief Слот клика по кнопке "О создателях". Создает экземпляр класса WindowAbout с информацией
     *        о разработчиках и отображает его.
     */
    void on_btn_about_clicked();

    /*!
     * \brief Слот клика по кнопке "Свернуть в трей". Отправляет сигнал inTrayMode().
     */
    void on_btn_tray_clicked();

    /*!
     * \brief Слот клика по кнопке "Редактировать команды". Создает экземпляр класса ComandsWindow
     *        и отображает его.
     */
    void on_pushButton_5_clicked();

    /*!
     * \brief Слот клика по кнопке "Сменить картинку". Создает экземпляр класса ImagesWindow
     *        и отображает его.
     */
    void on_pushButton_3_clicked();

    /*!
     * \brief Слот, отправляющий сигнал о смене главного изображения updateMainImage(). Срабатывает при
     *        получении сигнала
     */
    void sendToMoveWidget();

    /*!
     * \brief Слот клика по кнопке "Отображать поверх всех окон". Изменяет режим отображения главного окна
     *        и отправляет сигнал о изменении режима onTopModeChanged().
     */
    void on_btn_on_top_clicked();

    /*!
     * \brief Слот клика по кнопке "Сменить сочетание". Создает экземпляр класса SetCombination
     *        и отображает его.
     */
    void on_pushButton_4_clicked();

    /*!
     * \brief Слот отправляет сигнал об изменении сочетания клавиш для регистрации. Сигнал - registerKeys().
     */
    void on_pushButton_clicked();

    /*!
     * \brief Слот клика по кнопке "Терминал". Создает экземпляр класса Terminal
     *        и отображает его.
     */
    void on_btn_open_terminal_clicked();

    /*!
     * \brief Слот клика по кнопке "События". Создает экземпляр класса EventsWindow
     *        и отображает его.
     */
    void on_eventsButton_clicked();

    /*!
     * \brief Слот, отправляющий сигнал eventsDataChanged() об изменении списка событий в EventsWindow. Срабатывает при
     *        получении сигнала.
     */
    void eventsChangedToMainWin();

private:
    Ui::Menu *ui;
    QKeySequence keys;

signals:
    /*!
     * \brief Сигнал об изменении главного изображения. Отправлется в функции sendToMoveWidget().
     */
    void updateMainImage();

    /*!
     * \brief Сигнал о переходе в трей. Отправлется в функции on_btn_tray_clicked().
     */
    void inTrayMode();

    /*!
     * \brief Сигнал об изменении режима отображения главного окна. Отправлется в функции
     *        on_btn_on_top_clicked().
     */
    void onTopModeChanged();

    /*!
     * \brief Сигнал об изменении сочетания клавиш. Отправлется в функции on_pushButton_clicked().
     */
    void registerKeysMenu();

    /*!
     * \brief Сигнал об изменении данных о событиях. Отправлется в функции
     *        eventsChangedToMainWin().
     */
    void eventsDataChanged();
};

#endif // MENU_H
