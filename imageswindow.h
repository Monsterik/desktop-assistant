#ifndef IMAGESWINDOW_H
#define IMAGESWINDOW_H

#include <QDialog>
#include <QVector>

/*!
    \namespace Ui

    \brief Реализует связь класса с соответствующим ему ui. Особенность работы Qt.
*/
namespace Ui {
class ImagesWindow;
}


/*!
    \class ImagesWindow
    \brief ImagesWindow класс - окно с принципом работы, как
        у диалогового окна. Окно реализует работу с изображениями.

    Класс реализует работу с изображениями: методы удаления и выбора изображения
    как главного. Отображает все доступные для взаимодействия изображения.

*/
class ImagesWindow : public QDialog
{
    Q_OBJECT

public:
    /*!
      \fn ComandsWindow::ComandsWindow(QWidget *parent)

      Конструктор. Задаёт заголовок окна. Задаёт флаги окна. Инициализирует список
      изображений и загружает их в ui для отображения пользователю.

    */
    explicit ImagesWindow(QWidget *parent = nullptr);

    /*!
      \brief Деструктор класса ImagesWindow.

      Очищает выделенную в ходе работы память при удалении экземпляра.

    */
    ~ImagesWindow();

private slots:
    /*!
     * \brief Слот клика по кнопке "Добавить". Открывает обозреватель для выбора изображения, после
     *        чего копирует изображение в директорию ./images в корне приложения
     */
    void on_btn_add_img_clicked();

    /*!
     * \brief Слот клика по кнопке "Выбрать". Сохраняет данные о новом главном выбранном изображении в
     *          настройки QSettings и отправляет сигнал об изменении главного изображения.
     */
    void on_btn_choose_img_clicked();

    /*!
     * \brief Слот клика по кнопке "Удалить". Удаляет изображение из директории ./images и обновляет
     *      таблицу.
     */
    void on_btn_remove_clicked();

private:
    Ui::ImagesWindow *ui;
    QVector<QString> images_names;

signals:
    /*!
     * \brief Сигнал о изменении главного изображения. Отправляется в функции
     *        ImagesWindow::on_btn_choose_img_clicked()
     */
    void imgChanged();
};

#endif // IMAGESWINDOW_H
