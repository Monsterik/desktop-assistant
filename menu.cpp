#include "menu.h"
#include "ui_menu.h"
#include <QCoreApplication>
#include <QWidget>
#include "windowabout.h"
#include "mainwindow.h"
#include "comandswindow.h"
#include "imageswindow.h"
#include <QSettings>
#include "setcombination.h"
#include "terminal.h"
#include "eventswindow.h"
#include "QDebug"

/*!
  \fn ComandsWindow::ComandsWindow(QWidget *parent)

  Конструктор. Задаёт заголовок окна. Задаёт флаги окна. Инициализирует значение
  кнопки отображения поверх всех окон через настройки приложения (QSettings).

*/
Menu::Menu(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Menu)
{
    ui->setupUi(this);
    this->setWindowTitle("Десктопный помощник");
    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);

    QSettings settings("NikAndPasha", "desktopassistant");

    if (settings.value("isOnTop").toBool() == false)
    {
        ui->btn_on_top->setText("Отображать поверх всех окон");
    }
    else
    {
        ui->btn_on_top->setText("Не отображать поверх всех окон");
    }

    ui->Key_label->setText("Активация: CTRL + " + settings.value("keysS").toString());

}

Menu::~Menu()
{
    delete ui;
}

void Menu::on_btn_exit_clicked()
{
    QCoreApplication::quit();
}

void Menu::on_btn_about_clicked()
{
    WindowAbout *about_window = new WindowAbout();
    about_window->setModal(true);
    about_window->setVisible(true);
    about_window->show();
}



void Menu::on_pushButton_5_clicked()
{
    ComandsWindow *comands_window = new ComandsWindow();
    comands_window->setModal(true);
    comands_window->setVisible(true);
    comands_window->show();
}

void Menu::on_pushButton_3_clicked()
{
    ImagesWindow *img_window = new ImagesWindow();
    img_window->setModal(true);
    img_window->setVisible(true);
    img_window->show();
    connect(img_window, &ImagesWindow::imgChanged, this, &Menu::sendToMoveWidget);
}

void Menu::on_btn_tray_clicked()
{
    this->hide();
    emit inTrayMode();
}


void Menu::sendToMoveWidget()
{
    emit updateMainImage();
}

void Menu::on_btn_on_top_clicked()
{
    QSettings settings("NikAndPasha", "desktopassistant");

    if (settings.value("isOnTop").toBool() == false)
    {
        settings.setValue("isOnTop", true);
        ui->btn_on_top->setText("Не отображать поверх всех окон");
    }
    else
    {
        settings.setValue("isOnTop", false);
        ui->btn_on_top->setText("Отображать поверх всех окон");
    }

    emit onTopModeChanged();
}

void Menu::on_pushButton_4_clicked()
{
    SetCombination *comb_window = new SetCombination();
    comb_window->setModal(true);
    comb_window->setVisible(true);
    comb_window->show();
    connect(comb_window, &SetCombination::changeKeys, this, &Menu::changeKeys);
}

void Menu::changeKeys()
{
    QSettings settings("NikAndPasha", "desktopassistant");
    ui->Key_label->setText("Активация: CTRL + " + settings.value("keysS").toString());
    emit registerKeysMenu();
}

void Menu::on_pushButton_clicked()
{

}

void Menu::on_btn_open_terminal_clicked()
{
    Terminal *terminal_window = new Terminal();
    terminal_window->setModal(true);
    terminal_window->setVisible(true);
    terminal_window->show();
}

void Menu::on_eventsButton_clicked()
{
    EventsWindow *events_window = new EventsWindow();
    events_window->setModal(true);
    events_window->setVisible(true);
    events_window->show();
    connect(events_window, &EventsWindow::eventsDataChanged, this, &Menu::eventsChangedToMainWin);
}

void Menu::eventsChangedToMainWin()
{
    emit eventsDataChanged();
}
