#ifndef BOTANSWER_H
#define BOTANSWER_H

#include <QWidget>
#include <QTimer>

namespace Ui {
class BotAnswer;
}

class BotAnswer : public QWidget
{
    Q_OBJECT

public:
    explicit BotAnswer(QWidget *parent = nullptr);
    ~BotAnswer();

private:
    Ui::BotAnswer *ui;
    QTimer *close_timer;

private slots:
    void closeWindowTimerOut();

public:
    void showAnswerLabel();

};

#endif // BOTANSWER_H
