#include "comandswindow.h"
#include "ui_comandswindow.h"
#include "newcomandwindow.h"
#include <QFile>
#include <QtGui>
#include <QDebug>
#include "newcomandwindow.h"
#include <QListWidget>
#include <fstream>


void ComandsWindow::loadCommandsToList()
{
    QFile file_comands("./commands.txt");
    if (file_comands.exists())
    {
        if (file_comands.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            while (!file_comands.atEnd())
            {
                ui->listWidget->addItem(file_comands.readLine());
                file_comands.readLine();
            }

        }
    }
    file_comands.close();
}


ComandsWindow::ComandsWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ComandsWindow)
{
    ui->setupUi(this);
    this->setWindowTitle("Редактор команд");
    loadCommandsToList();
    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
}

ComandsWindow::~ComandsWindow()
{
    delete ui;
}

void ComandsWindow::updateList()
{
    ui->listWidget->clear();
    loadCommandsToList();
}

void ComandsWindow::on_btn_add_new_clicked()
{
    NewComandWindow *window = new NewComandWindow();
    window->setModal(true);
    window->setVisible(true);
    window->show();
    connect(window, &NewComandWindow::updateList, this, &ComandsWindow::updateList);
}

void ComandsWindow::on_btn_remove_clicked()
{
    QListWidgetItem *item = ui->listWidget->takeItem(ui->listWidget->currentRow());
    if (item != nullptr)
    {
        QString comand = item->text();
        delete item;

        // Удаление через перезапись с использованием временного файла
        std::ofstream tmp_file;
        QFile comand_file("./commands.txt");
        tmp_file.open("./tmp.txt", std::ios::app | std::ios::in);
        comand_file.open(QIODevice::ReadOnly | QIODevice::Text);
        while (!comand_file.atEnd())
        {
            QString read_str= comand_file.readLine();
            if (read_str != comand)
            {
                tmp_file << read_str.toStdString();
                tmp_file << comand_file.readLine().toStdString();
            }
            else
            {
                comand_file.readLine();
            }
        }
        comand_file.close();
        tmp_file.close();
        std::ofstream updated_file;
        updated_file.open("./commands.txt", std::ios::out);
        updated_file.clear();
        QFile tmp_read_file("./tmp.txt");
        tmp_read_file.open(QIODevice::ReadOnly | QIODevice::Text);

        tmp_read_file.seek(0);

        while (!tmp_read_file.atEnd())
        {
            updated_file << tmp_read_file.readLine().toStdString();
        }
        tmp_read_file.close();
        comand_file.close();
        remove("./tmp.txt");
    }
}
