#include "newcomandwindow.h"
#include "ui_newcomandwindow.h"
#include <QFileDialog>
#include <QDebug>
#include <iostream>
#include <fstream>


NewComandWindow::NewComandWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::NewComandWindow)
{
    ui->setupUi(this);
    this->setWindowTitle("Добавить команду");
    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
}

NewComandWindow::~NewComandWindow()
{
    delete ui;
}

void NewComandWindow::on_pushButton_clicked()
{
    QUrl path = QFileDialog::getOpenFileUrl(this, "Open file");
    QString path_str = path.toString();
    ui->url_line->setText(path_str);
}

void NewComandWindow::on_btn_open_folder_clicked()
{
    QUrl path = QFileDialog::getExistingDirectory(this, "Open directory", "",QFileDialog::ShowDirsOnly);
    QString path_str = path.toString();
    ui->url_line->setText(path_str);
}

void NewComandWindow::on_btn_save_comand_clicked()
{
    QString command = ui->lineEdit->text() + "\n" + ui->url_line->text() + "\n";

    std::ofstream comand_file;
    comand_file.open("./commands.txt", std::ios::app);
    comand_file << command.toStdString();
    comand_file.close();
    this->close();
}

void NewComandWindow::closeEvent(QCloseEvent *)
{
    emit updateList();
}
