#include "voicerecwindow.h"
#include "ui_voicerecwindow.h"
#include <QtMultimedia/QAudioRecorder>
#include <QtMultimedia/QAudioEncoderSettings>
#include <QUrl>
#include <QKeyEvent>
#include <QDebug>
#include <QFileInfo>
#include <QJsonDocument>
#include <QJsonObject>
#include <QFile>
#include <QByteArray>
#include <QNetworkRequest>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QDesktopServices>
#include <QMap>
#include "move_widget.h"
#include <QSettings>

VoiceRecWindow::VoiceRecWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::VoiceRecWindow)
{
    ui->setupUi(this);
    //this->setWindowTitle("Голосовое управление");
    //this->setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
    this->installEventFilter(this);

    QPixmap micImage(":/additional/mic.png");
    micImage = micImage.scaled(QSize(128, 128), Qt::IgnoreAspectRatio,Qt::SmoothTransformation);
    ui->label->setPixmap(micImage);

    //QSettings settings("NikAndPasha", "desktopassistant");

    this->setWindowFlag(Qt::FramelessWindowHint);
    ui->label->setAttribute(Qt::WA_TranslucentBackground,true);
    this->setAttribute(Qt::WA_TranslucentBackground,true);
    isRecordFinished = false;

    //this->move(settings.value("x").toFloat(),settings.value("y").toFloat());


    baseApi = "https://speech.googleapis.com/v1/speech:recognize";
    apiKey = "AIzaSyCLQQ8nXhuXWsz1PTPm_77hOMtZjU7v2ag";



    //Загружаем список команд с путями
    QFile file_comands("./commands.txt");
    mapWithUrls = new QMap<QString, QString>();
    if (file_comands.exists())
    {
        if (file_comands.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            while (!file_comands.atEnd())
            {
                QString command = file_comands.readLine();
                command.remove(command.back());
                command = command.toUpper();
                QString url = file_comands.readLine();
                url.remove(url.back());
                for (int i = 0; i < 8; ++i)
                    url.remove("file:///");
                mapWithUrls->insert(command, url);
            }
        }
    }
    file_comands.close();

    audioRecorder = new QAudioRecorder(this);
    audioSettings.setChannelCount(1);
    audioSettings.setSampleRate(44100);
    audioSettings.setCodec("audio/pcm");
    audioSettings.setQuality(QMultimedia::NormalQuality);

    audioRecorder->setEncodingSettings(audioSettings);

    audioRecorder->setOutputLocation(QUrl::fromLocalFile(QFileInfo("./command_voice").absoluteFilePath()));
    audioRecorder->record();

    connect(&close_timer, &QTimer::timeout, this, &VoiceRecWindow::closeWindowTimerOut);
    connect(&qam, &QNetworkAccessManager::finished, [this](QNetworkReply *response)
            {
                auto data = QJsonDocument::fromJson(response->readAll());
                response->deleteLater();

                qDebug() << data;

                auto error = data["error"]["message"];

                if (error.isUndefined()) {
                    auto command = data["results"][0]["alternatives"][0]["transcript"].toString();
                    QSettings settings("NikAndPasha", "desktopassistant");
                    settings.setValue("question", command);
                    //this->ui->label->setText(command);
                    commandToExec = command.toUpper();
                    close_timer.start(1);
                    if (command.toUpper() == "ВЫКЛЮЧИСЬ")
                    {
                        QCoreApplication::quit();
                    }

                } else {
                    qDebug() << "ERROR RECOGNITION";
                }
            });
}

VoiceRecWindow::~VoiceRecWindow()
{
    delete ui;
    delete audioRecorder;
    delete mapWithUrls;
}


bool VoiceRecWindow::eventFilter(QObject *obj, QEvent *event)
{
    if(event->type()==QEvent::KeyRelease) {

            //pressedKeys += ((QKeyEvent*)event)->key();
            qDebug() << ((QKeyEvent*)event)->key();
            //if( pressedKeys.contains(Qt::Key_D))
            if ((((QKeyEvent*)event)->key() == Qt::Key_D ||
                    ((QKeyEvent*)event)->key() == Qt::Key_Control)
                    && isRecordFinished == false)
            {
                qDebug() << "STOPED REC ";
                audioRecorder->stop();
                isRecordFinished = true;
                sendRecognitionRequest();
            }

        }
        else if(event->type()==QEvent::KeyPress)
        {

           //pressedKeys -= ((QKeyEvent*)event)->key();
            qDebug() << "NOT RELEASE";
        }


        return false;
}

void VoiceRecWindow::sendRecognitionRequest()
{
    QFile file("./command_voice.wav");

    if (file.open(QIODevice::ReadOnly))
    {
        QByteArray fileData = file.readAll();
        file.close();

        QJsonDocument data {
            QJsonObject { {
                    "audio",
                    QJsonObject { {"content", QJsonValue::fromVariant(fileData.toBase64())} }
                },  {
                    "config",
                    QJsonObject {
                        {"encoding", "LINEAR16"},
                        {"language_code", "ru-RU"},
                        {"model", "command_and_search"},
                        {"sample_rate_hertz", audioSettings.sampleRate()}
                    }}
            }
        };

        url.setUrl(this->baseApi);
        url.setQuery("key=" + this->apiKey);
        request.setUrl(url);
        request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
        request.setAttribute(QNetworkRequest::HTTP2AllowedAttribute, true);

        qam.post(request, data.toJson(QJsonDocument::Compact));

    }
    else
    {
        qDebug() << "OPENING FILE ERROR";
    }
}

void VoiceRecWindow::closeWindowTimerOut()
{
    QUrl command_url;
    QString path = mapWithUrls->value(commandToExec);

    if (path != "")
    {
        command_url.setUrl(path);
        bool isExec = QDesktopServices::openUrl(command_url);
        QSettings settings("NikAndPasha", "desktopassistant");
        settings.setValue("botAnswer", "Выполняю команду " + settings.value("question").toString());
        emit commandDetected();
    }
    else
    {
        emit hellodetected();
    }
    this->close();
    emit recognitionFinished();
}
