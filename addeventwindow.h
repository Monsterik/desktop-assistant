#ifndef ADDEVENTWINDOW_H
#define ADDEVENTWINDOW_H

#include <QDialog>

/*!
    \namespace Ui

    \brief Реализует связь класса с соответствующим ему ui. Особенность работы Qt.
*/
namespace Ui {
class AddEventWindow;
}


/*!
    \class AddEventWindow
    \brief AddEventWindow класс - окно с принципом работы, как
        у диалогового окна. В данном окне проходит процесс добавления
        нового события.


    Класс описывает окно добавления события и соответствующие для этого методы.
    Наследуется от QDialog класса. Внешний вид класса описан соответствующим ui.

*/
class AddEventWindow : public QDialog
{
    Q_OBJECT

public:
    /*!
      \brief Конструктор класса AddEventWindow.

      Задаёт заголовок окна. Задаёт флаги окна.

    */
    explicit AddEventWindow(QWidget *parent = nullptr);

    /*!
      \brief Деструктор класса AddEventWindow.

      Очищает выделенную в ходе работы память при удалении экземпляра.

    */
    ~AddEventWindow();

private slots:

    /*!
        \fn void on_btn_add_clicked()
        \brief Слот клика по кнопке "Добавить" из UI. Производит считывание
        введённых полей события из Ui и записывает новые данные в файл.
    */
    void on_btn_add_clicked();

    /*!
      \fn on_btn_cancel_clicked()
      \brief  Слот клика по кнопке "Отмена" из UI. Закрывает окно добавления команды
               без сохранения данных соответственно.
    */
    void on_btn_cancel_clicked();

signals:
    /*!
     * \brief updateTable signal.
     *      Отправляется после добавления данных в файл в функции-слоте
     *      AddEventWindow::btn_add_clicked()
     */
    void updateTable();

private:
    /// Доступ к элементам графического интерфейса
    Ui::AddEventWindow *ui;
};

#endif // ADDEVENTWINDOW_H
