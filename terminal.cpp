#include "terminal.h"
#include "ui_terminal.h"
#include <QCompleter>
#include <QListWidget>
#include <QFile>
#include <QDebug>
#include <QDesktopServices>
#include <QMap>

Terminal::Terminal(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Terminal)
{
    ui->setupUi(this);
    this->setWindowTitle("Терминал");
    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);


    //Загружаем список команд из файла для загрузки в Completer
    QListWidget* listWidget = new QListWidget();
    QFile file_comands("./commands.txt");
    if (file_comands.exists())
    {
        if (file_comands.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            while (!file_comands.atEnd())
            {
                QString string_from_file = file_comands.readLine();
                string_from_file.remove(string_from_file.back());
                listWidget->addItem(string_from_file);
                file_comands.readLine();
            }

        }
    }
    file_comands.close();
    QCompleter* completer = new QCompleter(this);
    completer->setModel(listWidget->model());
    completer->setCaseSensitivity(Qt::CaseInsensitive);

    //Загружаем список команд с путями
    mapWithUrls = new QMap<QString, QString>();
    if (file_comands.exists())
    {
        if (file_comands.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            while (!file_comands.atEnd())
            {
                QString command = file_comands.readLine();
                command.remove(command.back());
                QString url = file_comands.readLine();
                url.remove(url.back());
                for (int i = 0; i < 8; ++i)
                    url.remove("file:///");
                mapWithUrls->insert(command, url);
            }

        }
    }
    file_comands.close();
    ui->command_line->setCompleter(completer);
}

Terminal::~Terminal()
{
    delete ui;
    delete mapWithUrls;
}

void Terminal::on_btn_exec_command_clicked()
{
    QString command_from_line = ui->command_line->text();
    QUrl command_url;
    QString path = mapWithUrls->value(command_from_line);

    ui->terminal_browser->append("Вы: " + command_from_line);
    command_url.setUrl(path);

    bool isExec = QDesktopServices::openUrl(command_url);
    if (isExec == true)
    {
        ui->terminal_browser->append("Выполняю");
    }
    else
    {
        ui->terminal_browser->append("Настигла неудача при попытке выполнить команду");
    }

}
