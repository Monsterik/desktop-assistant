#include "hotkeycontroll.h"
#include "ui_hotkeycontroll.h"
#include "windows.h"
#include <QDebug>
#include "voicerecwindow.h"
#include <QSettings>
#include "mainwindow.h"

HotKeyControll::HotKeyControll(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::HotKeyControll)
{
    ui->setupUi(this);

    rec_win = nullptr;
    QSettings settings("NikAndPasha", "desktopassistant");
    RegisterHotKey((HWND)HotKeyControll::winId(),
                       100,
                       MOD_CONTROL,
                       settings.value("keys", 'R').toUInt());
}



HotKeyControll::~HotKeyControll()
{
    delete ui;
}

void HotKeyControll::registerKeys()
{
    QSettings settings("NikAndPasha", "desktopassistant");
    UnregisterHotKey((HWND)HotKeyControll::winId(), 100);
    RegisterHotKey((HWND)HotKeyControll::winId(),
                       100,
                       MOD_CONTROL,
                       settings.value("keys", 'R').toUInt());
}

bool HotKeyControll::nativeEvent(const QByteArray &eventType, void *message, long *result)
{
    Q_UNUSED(eventType)
    Q_UNUSED(result)

    MSG* msg = reinterpret_cast<MSG*>(message);

    if(msg->message == WM_HOTKEY){
        // Проверка кода из код hotkey
        if(msg->wParam == 100){
            if (rec_win == nullptr)
            {



                qDebug() << "RECWIN";
                rec_win = new VoiceRecWindow();

                connect(rec_win, &VoiceRecWindow::hellodetected, this, &HotKeyControll::sendHello);
                connect(rec_win, &VoiceRecWindow::commandDetected, this, &HotKeyControll::sendCommand);

                connect(rec_win, &VoiceRecWindow::recognitionFinished, this, &HotKeyControll::deleteRecWin);
                rec_win->setModal(true);
                rec_win->setFocus();
                rec_win->setVisible(true);
                rec_win->show();
                rec_win->raise();
                rec_win->activateWindow();
            }

            return true;
        }
    }
    return false;
}

void HotKeyControll::deleteRecWin()
{
    delete rec_win;
    rec_win = nullptr;
}

void HotKeyControll::sendHello()
{
    emit helloOpen();
}

void HotKeyControll::sendCommand()
{
    emit commandOpen();
}
