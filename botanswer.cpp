#include "botanswer.h"
#include "ui_botanswer.h"
#include <QSettings>
#include <QTime>

BotAnswer::BotAnswer(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::BotAnswer)
{
    ui->setupUi(this);
    this->setWindowFlag(Qt::FramelessWindowHint);
    this->setAttribute(Qt::WA_TranslucentBackground,true);
    this->setWindowFlag(Qt::WindowStaysOnTopHint);
    close_timer = new QTimer(this);
    connect(close_timer, &QTimer::timeout, this, &BotAnswer::closeWindowTimerOut);
}

BotAnswer::~BotAnswer()
{
    delete ui;
}

void BotAnswer::showAnswerLabel()
{
    QSettings settings("NikAndPasha", "desktopassistant");
    ui->label->setText(settings.value("botAnswer").toString());
    this->show();
    this->activateWindow();
    this->raise();
    close_timer->start(100 * settings.value("botAnswer").toString().length());
    //QTime dieTime = QTime::currentTime().addMSecs(5000);
    //while( QTime::currentTime() < dieTime )
    //{
    //    QCoreApplication::processEvents( QEventLoop::AllEvents, 100 );
    //}

}
void BotAnswer::closeWindowTimerOut()
{
    this->hide();
}
