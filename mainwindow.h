#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSystemTrayIcon>
#include "move_widget.h"
#include <QVector>
#include <QTimer>
#include <QJsonArray>
#include "hotkeycontroll.h"

#define SECS_IN_TWO_YEARS 31536000 * 2

/*!
    \namespace Ui

    \brief Реализует связь класса с соответствующим ему ui. Особенность работы Qt.
*/
namespace Ui {
class MainWindow;
}

/*!
    \class MainWindow
    \brief EventsWindow - класс, наследующийся от QMainWindow. Используется
        для создания главного окна приложения, с которого начинается взаимодействие
        пользователя и приложения.


*/
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    /*!
      \fn ComandsWindow::ComandsWindow(QWidget *parent)

      Конструктор. Задаёт заголовок окна. Задаёт флаги окна. Инициализирует
      из настроек QSettings главное изображение, задает в настройках поле
      isOnTop - режим отображения главного окна. Создаёт экземпляр класса окна меню.
      Инициализирует настройки трея, регистрирует hotkeys.
      Производит connect по сигналам: события трея, изменение главного изображения,
      истечение таймера, сигнал для сворачивания в трей, изменение настройки поля
      isOnTop, изменение списка событий, активация трея.

    */
    explicit MainWindow(QWidget *parent = nullptr);

    /*!
      \brief Деструктор класса MainWindow.

      Очищает выделенную в ходе работы память при удалении экземпляра.

    */
    ~MainWindow();
    Move_Widget* getLabel();

signals:
    void eventCame();

private slots:
    /*!
     * \brief Слот обработки клика по иконке трея.
     * \param reason
     */
    void iconActivated(QSystemTrayIcon::ActivationReason reason);

    /*!
     * \brief Слот изменения главного изображения при выборе нового в ImagesWindow
     */
    void updateMainImage();

    /*!
     * \brief Слот перехода в трей мод при получении сигнала из Menu.
     */
    void mainWindowToTrayMode();

    /*!
     * \brief Переход в режим отображения поверх всех окон при получении сигнала из Menu.
     */
    void changeOnTopMode();

    /*!
     * \brief Слот вывода уведомления о наступлении события при истечении установленного в
     *        конструкторе таймера.
     */
    void showEventMessage();

    /*!
     * \brief Слот удаления события из файла при закрытии окна уведомления о событии. Также
     *        производит смену таймера.
     */
    void eventNoticeClosed();

    /*!
     * \brief Слот загрузки данных о событиях. Выполняет функцию MainWindow::setClosestEventTimer().
     * \sa MainWindow::setClosestEventTimer()
     */
    void loadChangedEventsData();

    /*!
     * \brief Слот регистрирует новое сочетание клавиш.
     */
    void registerKeys();

private:
    Ui::MainWindow *ui;

    QSystemTrayIcon *trayIcon;

    Move_Widget *label = new Move_Widget();

    int indexOfCurrentEvent;

    QVector<QJsonArray> eventsJsons;

    QTimer closest_event_timer;

    HotKeyControll *hotkeyController;

    QMenu * menu;

    /*!
     * \brief Функция загружает данные о событиях из файла и устанавливает таймер на
     *        ближайшее событие.
    */
    void setClosestEventTimer();
signals:
    void RegisterKeys();
};

#endif // MAINWINDOW_H
