#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "menu.h"
#include <QStyle>
#include <QMenu>
#include <QSettings>
#include <QTimer>
#include <QDateTime>
#include <QDebug>
#include <QJsonArray>
#include <fstream>
#include "imageswindow.h"
#include "setcombination.h"
#include "eventnoticewindow.h"
#include "voicerecwindow.h"
#include "windows.h"
#include "hotkeycontroll.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    //Загрузка событий и установка таймера
    setClosestEventTimer();
    //------------------------------------

    QSettings settings("NikAndPasha", "desktopassistant");
    settings.setValue("isOnTop", false);
    QPixmap mainImage(settings.value("mainImage", ":/images/think.png").toString());
    mainImage = mainImage.scaled(QSize(320, 400), Qt::IgnoreAspectRatio,Qt::SmoothTransformation);
    label->setPixmap(mainImage);

    trayIcon = new QSystemTrayIcon(this);
    trayIcon->setIcon(QIcon(":/additional/ico.png"));

    hotkeyController = new HotKeyControll();
    menu = new QMenu(this);
    QAction * viewWindow = new QAction("Развернуть окно", this);
    QAction * quitAction = new QAction("Выход", this);

    connect(viewWindow, SIGNAL(triggered()), this, SLOT(show()));
    connect(quitAction, SIGNAL(triggered()), this, SLOT(close()));

    menu->addAction(viewWindow);
    menu->addAction(quitAction);

    connect(hotkeyController, &HotKeyControll::helloOpen, label, &Move_Widget::openWithHello);
    connect(hotkeyController, &HotKeyControll::commandOpen, label, &Move_Widget::openCommand);


    hotkeyController->setModal(false);
    hotkeyController->setVisible(false);

    trayIcon->setContextMenu(menu);
    trayIcon->show();


    connect(label, &Move_Widget::eventsDataChanged, this, &MainWindow::loadChangedEventsData);
    connect(&closest_event_timer, &QTimer::timeout, this, &MainWindow::showEventMessage);
    connect(trayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)),
                this, SLOT(iconActivated(QSystemTrayIcon::ActivationReason)));
    connect(label, &Move_Widget::updateMainImage, this, &MainWindow::updateMainImage);
    connect(label, &Move_Widget::inTrayMode, this, &MainWindow::mainWindowToTrayMode);
    connect(label, &Move_Widget::onTopModeChanged, this, &MainWindow::changeOnTopMode);
    connect(label, &Move_Widget::RegisterKeys, this, &MainWindow::registerKeys);

}

MainWindow::~MainWindow()
{
    delete ui;
    delete trayIcon;
    delete label;
}

void MainWindow::iconActivated(QSystemTrayIcon::ActivationReason reason)
{
    switch (reason){
    case QSystemTrayIcon::Trigger:
            if(!this->isVisible())
            {
                this->show();
            } else {
                this->hide();
            }
        break;
    default:
        break;
    }
}

void MainWindow::mainWindowToTrayMode()
{
    this->hide();
}


Move_Widget* MainWindow::getLabel()
{
    return this->label;
}

void MainWindow::updateMainImage()
{
    QSettings settings("NikAndPasha", "desktopassistant");
    QPixmap mainImage(settings.value("mainImage", ":/images/think.png").toString());
    mainImage = mainImage.scaled(QSize(320, 400), Qt::IgnoreAspectRatio,Qt::SmoothTransformation);
    label->setPixmap(mainImage);
}

void MainWindow::changeOnTopMode()
{
    QSettings settings("NikAndPasha", "desktopassistant");

    if (settings.value("isOnTop").toBool() == true)
    {
        this->setWindowFlag(Qt::WindowStaysOnTopHint);
        show();
    }
    else
    {
        this->setWindowFlag(Qt::WindowStaysOnTopHint, false);
        show();
    }

}

void MainWindow::setClosestEventTimer()
{
    if (!eventsJsons.empty())
    {
        eventsJsons.clear();
    }
    // Считывание событий из файла
    QFile file_events("./events.txt");
    int current_sec_timer = SECS_IN_TWO_YEARS;

    if (file_events.exists())
    {
        if (file_events.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            QDateTime current_date_time = QDateTime::currentDateTime();

            int eventsCounter = 0;

            while (!file_events.atEnd())
            {
                QString event_title = file_events.readLine();
                QString event_description = file_events.readLine();
                QString event_date = file_events.readLine();
                event_date = event_date.trimmed();

                QDateTime event_date_time = QDateTime::fromString(event_date,  "dd.MM.yyyy H:mm");

                if (event_title != nullptr && event_description != nullptr && event_date != nullptr)
                {
                    if (current_date_time.secsTo(event_date_time) > 0)
                    {
                        int tempSecs = current_date_time.secsTo(event_date_time);

                        if (tempSecs < current_sec_timer)
                        {
                            current_sec_timer = tempSecs;
                            this->indexOfCurrentEvent = eventsCounter;
                        }
                        QJsonArray newEvent = {event_title, event_description, event_date};
                        eventsJsons.push_back(newEvent);
                        eventsCounter++;
                    }
                }
            }

        }
    }
    file_events.close();

    if (current_sec_timer < SECS_IN_TWO_YEARS)
    {
        closest_event_timer.start(current_sec_timer * 1000);
    }
}

void MainWindow::showEventMessage()
{
    EventNoticeWindow *eventNotice = new EventNoticeWindow();
    eventNotice->setWindowData(eventsJsons[indexOfCurrentEvent].at(0).toString(), eventsJsons[indexOfCurrentEvent].at(1).toString());
    eventNotice->setModal(true);
    eventNotice->setVisible(true);
    eventNotice->show();
    closest_event_timer.stop();

    connect(eventNotice, &EventNoticeWindow::eventNoticeClosed, this, &MainWindow::eventNoticeClosed);
}

void MainWindow::eventNoticeClosed()
{
    eventsJsons.removeAt(indexOfCurrentEvent);

    std::ofstream event_file;
    event_file.open("./events.txt", std::ofstream::out | std::ofstream::trunc);
    for (int i = 0; i < eventsJsons.size(); ++i)
    {
        QString event_title = eventsJsons[i].at(0).toString();
        QString event_description = eventsJsons[i].at(1).toString();
        QString event_date = eventsJsons[i].at(2).toString();
        event_file << event_title.toStdString();
        event_file << event_description.toStdString();
        event_file << event_date.toStdString();
    }
    event_file.close();

    if (eventsJsons.size() > 0)
    {
        int current_sec_timer = SECS_IN_TWO_YEARS;

        for (int i = 0; i < eventsJsons.size(); ++i)
        {
            QString event_date = eventsJsons[i].at(2).toString();
            event_date = event_date.trimmed();

            QDateTime event_date_time = QDateTime::fromString(event_date,  "dd.MM.yyyy H:mm");
            QDateTime current_date_time = QDateTime::currentDateTime();

            int tempSecs = current_date_time.secsTo(event_date_time);

            if (tempSecs < current_sec_timer)
            {
                  current_sec_timer = tempSecs;
                  this->indexOfCurrentEvent = i;
            }
        }

        if (current_sec_timer != SECS_IN_TWO_YEARS)
        {
            closest_event_timer.start(current_sec_timer * 1000);
        }
    }
}

void MainWindow::loadChangedEventsData()
{
    setClosestEventTimer();
}

void MainWindow::registerKeys()
{
    hotkeyController->registerKeys();
}
