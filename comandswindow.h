#ifndef COMANDSWINDOW_H
#define COMANDSWINDOW_H

#include <QDialog>
#include <QFile>

/*!
    \namespace Ui

    \brief Реализует связь класса с соответствующим ему ui. Особенность работы Qt.
*/
namespace Ui {
class ComandsWindow;
}


/*!
    \class ComandsWindow
    \brief ComandsWindow класс - окно с принципом работы, как
        у диалогового окна. В данном окне проходит процесс работы
        с командами.


    Класс описывает окно работы с командами. Реализует возможность удаления
    команды, отображает все доступные команды. Реализует переход в окно добавления
    команды.

*/
class ComandsWindow : public QDialog
{
    Q_OBJECT

public:
    /*!
      \fn ComandsWindow::ComandsWindow(QWidget *parent)

      Конструктор. Задаёт заголовок окна. Задаёт флаги окна.
      Загружает события из файла в список с помощью loadCommandsToList().

      \sa loadCommandsToList()

    */
    explicit ComandsWindow(QWidget *parent = nullptr);

    /*!
      \brief Деструктор класса ComandsWindow.

      Очищает выделенную в ходе работы память при удалении экземпляра.

    */
    ~ComandsWindow();

private slots:
    /*!
        \brief Слот клика по кнопке "Добавить" из UI. Создаёт экземпляр
            класса окна NewComandWindow и отображает его.
    */
    void on_btn_add_new_clicked();

    /*!
        \brief Слот обновления таблицы по сигналу из NewComandWindow. Производит
        считывание измененного файла команд и загружает в виджет.
    */
    void updateList();

    /*!
        \brief Слот клика по кнопке "Удалить" из UI. Удаляет команду из виджета
            и перезаписывает измененные данные виджета в файл.
    */
    void on_btn_remove_clicked();

private:
    Ui::ComandsWindow *ui;

    /*!
        \brief Функция загрузки данных команд из файла
    */
    void loadCommandsToList();

};

#endif // COMANDSWINDOW_H
