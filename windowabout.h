#ifndef WINDOWABOUT_H
#define WINDOWABOUT_H

#include <QDialog>


/*!
    \namespace Ui

    \brief Реализует связь класса с соответствующим ему ui. Особенность работы Qt.
*/
namespace Ui {
class WindowAbout;
}


/*!
    \class WindowAbout
    \brief WindowAbout класс - окно с принципом работы, как
        у диалогового окна. Окно выводит информацию о разработчиках.

*/
class WindowAbout : public QDialog
{
    Q_OBJECT

public:
    /*!
      \fn WindowAbout::WindowAbout(QWidget *parent)

      Конструктор. Задаёт заголовок окна. Задаёт флаги окна.

    */
    explicit WindowAbout(QWidget *parent = nullptr);

    /*!
      \brief Деструктор класса WindowAbout.

      Очищает выделенную в ходе работы память при удалении экземпляра.

    */
    ~WindowAbout();

private:
    Ui::WindowAbout *ui;
};

#endif // WINDOWABOUT_H
