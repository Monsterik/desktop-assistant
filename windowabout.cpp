#include "windowabout.h"
#include "ui_windowabout.h"

WindowAbout::WindowAbout(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::WindowAbout)
{
    ui->setupUi(this);
    this->setWindowTitle("О создателях");
    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
}

WindowAbout::~WindowAbout()
{
    delete ui;
}
