#include "eventnoticewindow.h"
#include "ui_eventnoticewindow.h"


EventNoticeWindow::EventNoticeWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EventNoticeWindow)
{
    ui->setupUi(this);
    this->setWindowTitle("Уведомление о событии");
    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
    this->setWindowFlag(Qt::WindowStaysOnTopHint);
}

EventNoticeWindow::~EventNoticeWindow()
{
    delete ui;
}

void EventNoticeWindow::on_btn_ok_clicked()
{
    this->close();
}

void EventNoticeWindow::setWindowData(QString event_title, QString event_description)
{
    ui->label_event_title->setText(event_title);
    ui->text_event_decription->setText(event_description);
}


void EventNoticeWindow::closeEvent(QCloseEvent *event)
{
    emit eventNoticeClosed();
}
