#ifndef TERMINAL_H
#define TERMINAL_H

#include <QDialog>
#include <QListWidget>
#include <QMap>

/*!
    \namespace Ui

    \brief Реализует связь класса с соответствующим ему ui. Особенность работы Qt.
*/
namespace Ui {
class Terminal;
}


/*!
    \class Terminal
    \brief Terminal класс - окно с принципом работы, как
        у диалогового окна. В данном окне проходит процесс выполнения
        команд.

    Класс реализует окно выполнения команд. Ввод команды для выполнения,
    дополнение команды во время написания, исполенние команды, вывод
    результата выполнения (удача/неудача).

*/
class Terminal : public QDialog
{
    Q_OBJECT

public:
    /*!
      \fn Terminal::Terminal(QWidget *parent)

      Конструктор. Задаёт заголовок окна. Задаёт флаги окна. Загружает список команд
      из файла для загрузки в ui в соответствующий виджет. Инициализирует поле QMap
      с командами и путями через считывание из файла.

    */
    explicit Terminal(QWidget *parent = nullptr);

    /*!
      \brief Деструктор класса Terminal.

      Очищает выделенную в ходе работы память при удалении экземпляра.

    */
    ~Terminal();

private slots:
    /*!
     * \brief Слот клика по кнопке "Выполнить". Ищет путь по команде и выполняет его,
     *        если нашёл.
     */
    void on_btn_exec_command_clicked();

private:
    Ui::Terminal *ui;
    QMap<QString, QString> *mapWithUrls;
};

#endif // TERMINAL_H
