#include "imageswindow.h"
#include "ui_imageswindow.h"
#include <QFile>
#include <QDir>
#include <QDebug>
#include <QFileDialog>
#include <QResource>
#include <QSettings>


ImagesWindow::ImagesWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ImagesWindow)
{
    ui->setupUi(this);
    this->setWindowTitle("Выбор картинки");
    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
    QDir directory(":/images");
    QStringList images = directory.entryList(QStringList() << "*.png" << "*.PNG",QDir::Files);
    if (!images.empty())
    {
        foreach(QString filename, images)
        {
            QListWidgetItem *item = new QListWidgetItem();
            item->setIcon(QIcon(":/images/" + filename));
            ui->listWidget->setViewMode(QListWidget::IconMode);
            ui->listWidget->setIconSize(QSize(150, 200));
            ui->listWidget->setResizeMode(QListWidget::Adjust);
            images_names.push_back(":/images/" + filename);
            ui->listWidget->addItem(item);
        }
    }

    directory.setPath("./images/");
    QStringList images2 = directory.entryList(QStringList() << "*.png" << "*.PNG", QDir::Files);
    if (!images.empty())
    {
        foreach(QString filename, images2)
        {
            QListWidgetItem *item = new QListWidgetItem();
            item->setIcon(QIcon("./images/" + filename));
            images_names.push_back("./images/" + filename);
            ui->listWidget->addItem(item);
        }
    }
}

ImagesWindow::~ImagesWindow()
{
    delete ui;
}

void ImagesWindow::on_btn_add_img_clicked()
{
    QString filePath = QFileDialog::getOpenFileName(this, "Open file");
    QDir fileDir = QFileInfo(filePath).absoluteDir();
    QString fileName = QFileInfo(filePath).fileName();
    QString absPath = fileDir.absolutePath();
    QDir dir("./images");
    if (!dir.exists())
        dir.mkpath("./");
    QString folderPath = dir.absolutePath();
    QFile::copy(absPath + "/" + fileName, folderPath + "/new" + fileName);

    QListWidgetItem *item = new QListWidgetItem();
    item->setIcon(QIcon(folderPath + "/new" + fileName));

    images_names.push_back(folderPath + "/new" + fileName);
    ui->listWidget->addItem(item);
}

void ImagesWindow::on_btn_choose_img_clicked()
{
    QSettings settings("NikAndPasha", "desktopassistant");
    QString path = images_names.at(ui->listWidget->currentRow());

    if (!QFile(path).exists())
           path = images_names.at(ui->listWidget->currentRow());

    settings.setValue("mainImage", path);
    emit imgChanged();
}

void ImagesWindow::on_btn_remove_clicked()
{
    QSettings settings("NikAndPasha", "desktopassistant");
    QString path = images_names.at(ui->listWidget->currentRow());

    QString main_img = settings.value("mainImage").toString();
    if (path != main_img)
    {
        if (QFile(path).exists() && path[0] != ":")
        {
            QFile::remove(path);
            QListWidgetItem *item = ui->listWidget->takeItem(ui->listWidget->currentRow());
            images_names.remove(ui->listWidget->currentRow());
            delete item;
        }
    }
}
