#ifndef EVENTNOTICEWINDOW_H
#define EVENTNOTICEWINDOW_H

#include <QDialog>

/*!
    \namespace Ui

    \brief Реализует связь класса с соответствующим ему ui. Особенность работы Qt.
*/
namespace Ui {
class EventNoticeWindow;
}


/*!
    \class EventNoticeWindow
    \brief EventNoticeWindow класс - окно с принципом работы, как
        у диалогового окна. Данное окно отвечает за отображение уведомления,
        выводящего данные о наступившем запланированном событии.


    Класс описывает окно уведомления о наступившем запланированном событии.

*/
class EventNoticeWindow : public QDialog
{
    Q_OBJECT

public:
    /*!
      \fn EventNoticeWindow::EventNoticeWindow(QWidget *parent)

      Конструктор. Задаёт заголовок окна. Задаёт флаги окна.

    */
    explicit EventNoticeWindow(QWidget *parent = nullptr);

    /*!
      \brief Деструктор класса EventNoticeWindow.

      Очищает выделенную в ходе работы память при удалении экземпляра.

    */
    ~EventNoticeWindow();

    /*!
      \brief Функция заlаёт \a event_title И \a event_description соответствующих
             label в ui.

    */
    void setWindowData(QString event_title, QString event_description);

signals:
    /*!
     * \brief Сигнал отправляется при событии закрытия окна.
     */
    void eventNoticeClosed();

private slots:

    /*!
     * \brief Слот кнопки "Ок". Выполняет закрытие окна.
     */
    void on_btn_ok_clicked();

private:
    Ui::EventNoticeWindow *ui;

    /*!
     * \brief closeEvent - событие окна, поставляемое библиотекой Qt.
     * \param event
     */
    void closeEvent(QCloseEvent *event);

};

#endif // EVENTNOTICEWINDOW_H
