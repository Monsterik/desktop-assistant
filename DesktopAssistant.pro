QT       += core gui
QT += multimedia
QT += network
QT += networkauth

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

#################### OpenSSL#############################

#########################################################

SOURCES += \
    addeventwindow.cpp \
    autooauth2.cpp \
    botanswer.cpp \
    chatbot.cpp \
    comandswindow.cpp \
    eventnoticewindow.cpp \
    eventswindow.cpp \
    goauth2.cpp \
    hotkeycontroll.cpp \
    imageswindow.cpp \
    main.cpp \
    mainwindow.cpp \
    menu.cpp \
    move_widget.cpp \
    newcomandwindow.cpp \
    setcombination.cpp \
    terminal.cpp \
    voicerecwindow.cpp \
    windowabout.cpp

HEADERS += \
    addeventwindow.h \
    autooauth2.h \
    botanswer.h \
    chatbot.h \
    comandswindow.h \
    eventnoticewindow.h \
    eventswindow.h \
    goauth2.h \
    hotkeycontroll.h \
    imageswindow.h \
    mainwindow.h \
    menu.h \
    move_widget.h \
    newcomandwindow.h \
    setcombination.h \
    terminal.h \
    voicerecwindow.h \
    windowabout.h

TRANSLATIONS += \
    DesktopAssistant_ru_RU.ts

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    images.qrc

DISTFILES += \
    images/lambda.png \
    images/think.png

FORMS += \
    addeventwindow.ui \
    botanswer.ui \
    chatbot.ui \
    comandswindow.ui \
    eventnoticewindow.ui \
    eventswindow.ui \
    hotkeycontroll.ui \
    imageswindow.ui \
    mainwindow.ui \
    menu.ui \
    newcomandwindow.ui \
    setcombination.ui \
    terminal.ui \
    voicerecwindow.ui \
    windowabout.ui
