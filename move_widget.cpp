#include "move_widget.h"
#include <QCoreApplication>
#include <QWidget>
#include <QMouseEvent>
#include "menu.h"
#include "hotkeycontroll.h"
#include "setcombination.h"
#include "terminal.h"
#include "chatbot.h"
#include "voicerecwindow.h"
#include <QSettings>
#include <QDebug>
#include "botanswer.h"

Move_Widget::Move_Widget(QWidget *parent) :
    QLabel(parent)
{
    chatbot = new ChatBot();
    chatbot->setVisible(false);
    connect(this, &Move_Widget::micInput, chatbot, &ChatBot::inputOnMic);
    botAnswer = new BotAnswer();
    connect(chatbot, &ChatBot::showAnswer, botAnswer, &BotAnswer::showAnswerLabel);
    connect(this, &Move_Widget::showAnswer, botAnswer, &BotAnswer::showAnswerLabel);
    botAnswer->hide();
}

void Move_Widget::mouseMoveEvent(QMouseEvent *event)
{
    if(event->buttons() & Qt::LeftButton){
        float x,y;
        x=event->pos().x()-mpos.x()+this->parentWidget()->geometry().left()-this->geometry().left();
        y=event->pos().y()-mpos.y()+this->parentWidget()->geometry().top()-this->geometry().top();

        //chatbot->move(x - 300, y - 50);
        botAnswer->move(x - 100, y - 50);

        this->parentWidget()->move(x,y);
    }

}



void Move_Widget::mouseDoubleClickEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton)
    {
        Menu *menu_window = new Menu();
        menu_window->setModal(true);
        menu_window->setVisible(true);
        menu_window->show();
        connect(menu_window, &Menu::updateMainImage, this, &Move_Widget::sendToMainWindow);
        connect(menu_window, &Menu::inTrayMode, this, &Move_Widget::inTrayModeToMainWindow);
        connect(menu_window, &Menu::onTopModeChanged, this, &Move_Widget::onTopModeChangedToMainWindow);
        connect(menu_window, &Menu::eventsDataChanged, this, &Move_Widget::sendReloadEventsQuery);
        connect(menu_window, &Menu::registerKeysMenu, this, &Move_Widget::newKeysRegister);
    }
}



void Move_Widget::inTrayModeToMainWindow()
{
    emit inTrayMode();
}

void Move_Widget::sendToMainWindow()
{
    emit updateMainImage();
}

void Move_Widget::openWithHello()
    {
        //botAnswer->show();
        //botAnswer->activateWindow();
        //botAnswer->raise();
        emit micInput();
    }

void Move_Widget::openCommand()
    {
        emit showAnswer();
    }

void Move_Widget::mousePressEvent(QMouseEvent *event){
    if(event->button() == Qt::RightButton)
    {
        int x,y;
        x=this->parentWidget()->geometry().left()-this->geometry().left();
        y=this->parentWidget()->geometry().top()-this->geometry().top();
        chatbot->move(x - 300, y - 50);
        if (chatShowed)
        {
            chatbot->setWindowFlag(Qt::WindowStaysOnTopHint, false);
            chatShowed = false;
            chatbot->hide();
        }
        else
        {
            chatShowed = true;
            chatbot->setWindowFlag(Qt::WindowStaysOnTopHint);
            chatbot->show();
        }
    }
    else
        mpos = event->pos();
}


void Move_Widget::onTopModeChangedToMainWindow()
{
    emit onTopModeChanged();
}

void Move_Widget::sendReloadEventsQuery()
{
    emit eventsDataChanged();
}

void Move_Widget::newKeysRegister()
{
    emit RegisterKeys();
}
