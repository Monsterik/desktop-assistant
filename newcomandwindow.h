#ifndef NEWCOMANDWINDOW_H
#define NEWCOMANDWINDOW_H

#include <QDialog>

/*!
    \namespace Ui

    \brief Реализует связь класса с соответствующим ему ui. Особенность работы Qt.
*/
namespace Ui {
class NewComandWindow;
}



/*!
    \class NewComandWindow
    \brief NewComandWindow класс - окно с принципом работы, как
        у диалогового окна. В данном окне проходит процесс добавления
        новой команды. Значения полей получает из своегго ui.

*/
class NewComandWindow : public QDialog
{
    Q_OBJECT

public:
    /*!
      \fn NewComandWindow::NewComandWindow(QWidget *parent)

      Конструктор. Задаёт заголовок окна. Задаёт флаги окна.

    */
    explicit NewComandWindow(QWidget *parent = nullptr);

    /*!
      \brief Деструктор класса NewComandWindow.

      Очищает выделенную в ходе работы память при удалении экземпляра.

    */
    ~NewComandWindow();

private slots:
    /*!
     * \brief Слот клика по кнопке "Выбрать файл". Открывает обозреватель для выбора файла,
     *        выбранный путь устанавливает в соответствующем виджете в UI.
     */
    void on_pushButton_clicked();

    /*!
     * \brief Слот клика по кнопке "Выбрать папку". Открывает обозреватель для выбора директории,
     *        выбранный путь устанавливает в соответствующем виджете в UI.
     */
    void on_btn_open_folder_clicked();

    /*!
     * \brief Слот клика по кнопке "Добавить". Добавляет новые данные в файл.
    */
    void on_btn_save_comand_clicked();

    /*!
     * \brief Слот события закрытия окна. Отправляет сигнал updateList() об обновлении списка команд.
     */
    void closeEvent(QCloseEvent *);

private:
    Ui::NewComandWindow *ui;

signals:
    /*!
     * \brief Сигнал об обновлении списка событий. Отправляется в функции closeEvent().
     */
    void updateList();
};

#endif // NEWCOMANDWINDOW_H
