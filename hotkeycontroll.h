#ifndef HOTKEYCONTROLL_H
#define HOTKEYCONTROLL_H

#include <QDialog>
#include "voicerecwindow.h"

namespace Ui {
class HotKeyControll;
}

/*!
    \class HotKeyControll
    \brief HotKeyControll класс - окно с принципом работы, как
        у диалогового окна. В данном окне проходит отслеживание
        глобального нажатия клавиш.


    Класс описывает окно отслеживания глобального нажатия клавиш в фоновом режиме.
    Наследуется от QDialog класса. Внешний вид класса описан соответствующим ui.

*/
class HotKeyControll : public QDialog
{
    Q_OBJECT

protected:
    bool nativeEvent(const QByteArray &eventType, void *message, long *result);



public:
    /*!
          \fn HotKeyControll::HotKeyControll(QWidget *parent)

          Конструктор. Задаёт заголовок окна. Задаёт флаги окна. Конструктор. Задаёт заголовок окна.
          Задаёт флаг   и окна. Регистрирует
    */
    explicit HotKeyControll(QWidget *parent = nullptr);
    /*!
      \brief Деструктор класса HotKeyControll.

      Очищает выделенную в ходе работы память при удалении экземпляра.

    */
    ~HotKeyControll();
    /*!
     * \brief Вызов изменения сочетания клавиш.
     */
    void registerKeys();
    void sendCommand();

signals:

    void helloOpen();
    void commandOpen();


private slots:
    /*!
     * \brief Слот удаляет объект VoiceRecWin при получении от него сигнала.
     */
    void deleteRecWin();

    void sendHello();


private:
    Ui::HotKeyControll *ui;
    VoiceRecWindow *rec_win;
};

#endif // HOTKEYCONTROLL_H
