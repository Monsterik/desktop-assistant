#include "eventswindow.h"
#include "ui_eventswindow.h"
#include <QFile>
#include "addeventwindow.h"
#include <fstream>
#include <QDebug>
#include <QSizePolicy>

EventsWindow::EventsWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EventsWindow)
{
    ui->setupUi(this);
    this->setWindowTitle("События");
    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);

    ui->events_table->setColumnCount(3);
    ui->events_table->setHorizontalHeaderLabels(QStringList() << "Событие" << "Описание" << "Дата и время");
    ui->events_table->setColumnWidth(0, ui->events_table->width()/3);
    ui->events_table->setColumnWidth(1, ui->events_table->width()/3);
    ui->events_table->setColumnWidth(2, ui->events_table->width()/3);

    loadEventsToList();
    ui->events_table->resizeColumnsToContents();
}

EventsWindow::~EventsWindow()
{
    delete ui;
}


void EventsWindow::loadEventsToList()
{
    QFile file_events("./events.txt");
    if (file_events.exists())
    {
        if (file_events.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            int counter = 0;

            while (!file_events.atEnd())
            {
                QString event_title = file_events.readLine();
                QString event_description = file_events.readLine();
                QString event_date = file_events.readLine();

                if (event_title != nullptr && event_description != nullptr && event_date != nullptr)
                {
                    ui->events_table->insertRow(counter);

                    QTableWidgetItem *item_title = new QTableWidgetItem;
                    item_title->setText(event_title);
                    ui->events_table->setItem(counter, 0, item_title);

                    QTableWidgetItem *item_description = new QTableWidgetItem;
                    item_description->setText(event_description);
                    ui->events_table->setItem(counter, 1, item_description);

                    QTableWidgetItem *item_date = new QTableWidgetItem;
                    item_date->setText(event_date);
                    ui->events_table->setItem(counter, 2, item_date);

                    counter++;
                }
            }

        }
    }
    file_events.close();
}

void EventsWindow::updateTable()
{
    while (ui->events_table->rowCount() != 0)
    {
        ui->events_table->removeRow(0);
    }
    loadEventsToList();

    ui->events_table->resizeColumnsToContents();
    emit eventsDataChanged();

}

void EventsWindow::on_btn_add_event_clicked()
{
    AddEventWindow *window = new AddEventWindow();
    window->setModal(true);
    window->setVisible(true);
    window->show();
    connect(window, &AddEventWindow::updateTable, this, &EventsWindow::updateTable);
}

void EventsWindow::on_btn_edit_event_clicked()
{
    std::ofstream events_file;
    events_file.open("./events.txt", std::ofstream::out | std::ofstream::trunc);
    QString stringToFile;

    for (int i = 0; i < ui->events_table->rowCount(); ++i)
    {
        stringToFile = ui->events_table->item(i, 0)->text();
        events_file << stringToFile.toStdString();

        stringToFile = ui->events_table->item(i, 1)->text();
        events_file << stringToFile.toStdString();

        stringToFile = ui->events_table->item(i, 2)->text();
        events_file << stringToFile.toStdString();
    }
    events_file.close();
    emit eventsDataChanged();
}

void EventsWindow::on_btn_remove_event_clicked()
{        
    if (ui->events_table->selectedItems().length() > 0)
    {
        int selectedRow = ui->events_table->selectedItems()[0]->row();
        if (selectedRow != -1)
        {
            ui->events_table->removeRow(selectedRow);

            std::ofstream events_file;
            events_file.open("./events.txt", std::ofstream::out | std::ofstream::trunc);
            QString stringToFile;

            for (int i = 0; i < ui->events_table->rowCount(); ++i)
            {
                stringToFile = ui->events_table->item(i, 0)->text();
                events_file << stringToFile.toStdString();

                stringToFile = ui->events_table->item(i, 1)->text();
                events_file << stringToFile.toStdString();

                stringToFile = ui->events_table->item(i, 2)->text();
                events_file << stringToFile.toStdString();
            }
            events_file.close();
        }
        emit eventsDataChanged();
    }
}
