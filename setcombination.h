#ifndef SETCOMBINATION_H
#define SETCOMBINATION_H

#include <QDialog>
#include <QKeySequenceEdit>
#include <QKeyEvent>

/*!
    \namespace Ui

    \brief Реализует связь класса с соответствующим ему ui. Особенность работы Qt.
*/
namespace Ui {
class SetCombination;
}

class QKeyEvent;


/*!
    \class SetCombination
    \brief SetCombination класс - окно с принципом работы, как
        у диалогового окна. Окно задания комбинации клавиш для
        активации управления голосом.

*/
class SetCombination : public QDialog
{
    Q_OBJECT

public:
    /*!
      \fn SetCombination::SetCombination(QWidget *parent)

      Конструктор. Задаёт заголовок окна. Задаёт флаги окна.

    */
    explicit SetCombination(QWidget *parent = nullptr);
    /*!
      \brief Деструктор класса SetCombination.

      Очищает выделенную в ходе работы память при удалении экземпляра.

    */
    ~SetCombination();

private slots:
    /*!
     * \brief Слот события изменения поля сочетания клавиш в ui. Устанавливает новое сочетание
     *        \a keySequence в настройках.
     * \param keySequence
     */
    void on_keySequenceEdit_keySequenceChanged(const QKeySequence &keySequence);

private:
    Ui::SetCombination *ui;

signals:
    /*!
     * \brief Сигнал об изменении сочетания клавиш. Отправляется в функции on_keySequenceEdit_keySequenceChanged().
     */
    void changeKeys();
};


#endif // SETCOMBINATION_H
