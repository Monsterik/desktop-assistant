#include "addeventwindow.h"
#include "ui_addeventwindow.h"
#include <QFile>
#include <fstream>


AddEventWindow::AddEventWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddEventWindow)
{
    ui->setupUi(this);
    this->setWindowTitle("Новое событие");
    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
}

AddEventWindow::~AddEventWindow()
{
    delete ui;
}


void AddEventWindow::on_btn_add_clicked()
{
    QString event_title = ui->line_edit_title->text();
    QString event_description = ui->text_edit_description->toPlainText();
    QString event_date = ui->date_time_edit->text();

    std::ofstream event_file;
    event_file.open("./events.txt", std::ios::app);
    event_file << event_title.toStdString() << "\n";
    event_file << event_description.toStdString() << "\n";
    event_file << event_date.toStdString() << "\n";
    event_file.close();
    emit updateTable();
    this->close();
}

void AddEventWindow::on_btn_cancel_clicked()
{
    this->close();
}
