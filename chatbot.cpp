#include "chatbot.h"
#include "ui_chatbot.h"
#include <QNetworkRequest>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QDesktopServices>
#include <QUrl>
#include <QNetworkAccessManager>
#include <QTimer>
#include "goauth2.h"
#include "goauth2.h"
#include "QMediaPlayer"
#include "QAudioFormat"
#include "QAudioDeviceInfo"
#include "QAudioOutput"

ChatBot::ChatBot(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ChatBot)
{
    ui->setupUi(this);
    //this->setWindowFlags(Qt::Window | Qt::FramelessWindowHint);

    QSettings settings("NikAndPasha", "desktopassistant");



//    connect(&close_timer, &QTimer::timeout, this, &ChatBot::closeWindowTimerOut);
    connect(&qam, &QNetworkAccessManager::finished, [this](QNetworkReply *response)
            {
                QJsonDocument data = QJsonDocument::fromJson(response->readAll());

                //qDebug() << data;

                qDebug() << "Ответ:";
                qDebug() << data["queryResult"]["fulfillmentText"];
                //qDebug() << "Звук: ";
                //qDebug() << data["outputAudio"];

                auto error = data["error"]["message"];

                if (error.isUndefined()) {
                    auto command = data["queryResult"]["fulfillmentText"].toString();
                    ui->YourLabel->setText(inputText);
                    ui->BotLabel->setText(command);
                    QSettings settings("NikAndPasha", "desktopassistant");
                    //commandToExec = command.toUpper();
                    if (command != "")
                    {
                        settings.setValue("botAnswer", command);
                    }
                    else
                    {
                        settings.setValue("botAnswer", "Не знаю что вам ответить.");
                    }
                    //close_timer.start(1);
                    emit showAnswer();
                }
                else
                {
                    qDebug() << "ERROR CHATTING";
                    QSettings settings("NikAndPasha", "desktopassistant");
                    settings.setValue("botAnswer", "Не могли бы вы повторить?");
                    emit showAnswer();
                    ui->YourLabel->setText(inputText);
                    ui->BotLabel->setText("Не могли бы вы повторить?");
                }
            });
}

ChatBot::~ChatBot()
{
    delete ui;
}

void ChatBot::inputOnMic()
{
    QSettings settings("NikAndPasha", "desktopassistant");

    botRequest(settings.value("question").toString());

}

void ChatBot::on_Input_Button_clicked()
{
    botRequest(ui->Input->text());

}

void ChatBot::botRequest(QString input)
{
    inputText = input;
    QJsonDocument data {
        QJsonObject { {
                "query_input",
                QJsonObject{{ "text" ,
                        QJsonObject{{"text", input},
                                   { "language_code" , "ru-RU" }}},
                            } }
        }
    };


    QSettings settings("NikAndPasha", "desktopassistant");

    QString tok = settings.value("token", "R").toString();

    qDebug() << "TOKEN: " + tok;

    url.setUrl("https://dialogflow.googleapis.com/v2/projects/small-talk-ecsm/agent/sessions/1:detectIntent");
    request.setUrl(url);
    request.setRawHeader(QByteArray("Authorization"), QByteArray("Bearer " + tok.toUtf8()));
    request.setAttribute(QNetworkRequest::HTTP2AllowedAttribute, true);

    qam.post(request, data.toJson(QJsonDocument::Compact));
}

void ChatBot::closeWindowTimerOut()
{
    //QSettings settings("NikAndPasha", "desktopassistant");
    //QUrl command_url;
    //QString path = mapWithUrls->value(commandToExec);
    //if (path != "")
    //{
    //    command_url.setUrl(path);
    //    QDesktopServices::openUrl(command_url);
    //    settings.setValue("botAnswer", "Выполняю команду " + commandToExec);
    //}
    //emit showAnswer();
}
