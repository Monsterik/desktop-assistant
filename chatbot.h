#ifndef CHATBOT_H
#define CHATBOT_H

#include <QDialog>
#include <QMediaRecorder>
#include <QUrl>
#include <QKeyEvent>
#include <QDebug>
#include <QFileInfo>
#include <QJsonDocument>
#include <QJsonObject>
#include <QFile>
#include <QByteArray>
#include <QNetworkRequest>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QDesktopServices>
#include <QMap>
#include <QTimer>

namespace Ui {
class ChatBot;
}

class ChatBot : public QDialog
{
    Q_OBJECT

public:
    explicit ChatBot(QWidget *parent = nullptr);
    void inputOnMic();
    ~ChatBot();

private slots:
    void on_Input_Button_clicked();
    void closeWindowTimerOut();

private:
    Ui::ChatBot *ui;
    QString baseApi;
    QString apiKey;
    QNetworkAccessManager qam;
    QNetworkRequest request;
    QUrl url;
    bool isRecordFinished;
    QMap<QString, QString>* mapWithUrls;
    QString commandToExec;
    QString inputText;
    QTimer close_timer;
    void botRequest(QString input);

signals:
    void showAnswer();
};

#endif // CHATBOT_H
