#ifndef VOICERECWINDOW_H
#define VOICERECWINDOW_H

#include <QDialog>
#include <QAudioRecorder>
#include <QNetworkAccessManager>
#include <QTimer>

/*!
    \namespace Ui

    \brief Реализует связь класса с соответствующим ему ui. Особенность работы Qt.
*/
namespace Ui {
class VoiceRecWindow;
}


/*!
    \class VoiceRecWindow
    \brief VoiceRecWindow класс - окно с принципом работы, как
        у диалогового окна. В данном окне проходит процесс работы
        с распознаванием голоса.


    Класс описывает окно, которое вызывается для управления голосом.
    Реализует запись голоса в файл, отправку файла на для распознавания на сервер,
    вывод и выполенние итоговой команды.

*/
class VoiceRecWindow : public QDialog
{
    Q_OBJECT

public:
    /*!
      \fn VoiceRecWindow::VoiceRecWindow(QWidget *parent)

        Конструктор. Задаёт заголовок окна. Задаёт флаги окна. Инициализирует
        данные настроек для рекордера,  строки для формирования запроса (api ссылка
        и ключ).
        Производит connect по сигналам: получение ответа от сервера.


    */
    explicit VoiceRecWindow(QWidget *parent = nullptr);

    /*!
      \brief Деструктор класса VoiceRecWindow.

      Очищает выделенную в ходе работы память при удалении экземпляра.

    */
    ~VoiceRecWindow();

signals:
    /*!
     * \brief Сигнал о завершении распознавания голоса. Отправляется после завершения распознавания,
     *        выполнения команды и закрытия окна. Отправляется в функции closeWindowTimerOut().
     */
    void recognitionFinished();

    void hellodetected();

    void commandDetected();

private slots:
    /*!
     * \brief Слот завершения таймера. Выполняет распознанную команду, закрывает окно и отправляет
     *        сигнал recognitionFinished().
     */
    void closeWindowTimerOut();

private:
    Ui::VoiceRecWindow *ui;

    QSet<int> pressedKeys;

    QAudioRecorder *audioRecorder;
    QAudioEncoderSettings audioSettings;
    QString baseApi;
    QString apiKey;
    QNetworkAccessManager qam;
    QNetworkRequest request;
    QUrl url;
    QTimer close_timer;
    bool isRecordFinished;

    QMap<QString, QString>* mapWithUrls;
    QString commandToExec;


    bool eventFilter(QObject *obj, QEvent *event);
    void sendRecognitionRequest();

};

#endif // VOICERECWINDOW_H
