#include <QtWidgets>
#include <menu.h>
#include <mainwindow.h>
#include "move_widget.h"
#include <QCompleter>
#include "goauth2.h"
#include <QApplication>
#include <QDebug>
#include <QSettings>

/*!
 * \brief qMain - точка входа, создаются объекты приложения и главного окна.
 * \param argc
 * \param argv
 * \return
 */
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow window;
    window.setWindowFlag(Qt::FramelessWindowHint);
    window.setAttribute(Qt::WA_TranslucentBackground);

    QSettings settings("NikAndPasha", "desktopassistant");

    QFile credFile("D:/Labs/Desktop Assistant/desktop-assistant/credentials.json");
    credFile.open(QIODevice::ReadOnly | QIODevice::Text);
    GOAuth2 gCal(QJsonDocument::fromJson(credFile.readAll()));
    credFile.close();



    gCal.connect(&gCal, &GOAuth2::statusChanged,
                      [&](GOAuth2::Status status){
            switch (status) {
            case QAbstractOAuth::Status::Granted:
                qDebug() << "Granted!";
                qDebug() <<"Authentication succeeded!\n Expire at "
                                            + gCal.expirationAt().toString();
                qDebug() << "Access Token: " + gCal.token();
                settings.setValue("token", gCal.token());

                qDebug() << "Refresh Token: " + gCal.refreshToken();
                settings.setValue("refreshToken", gCal.refreshToken());
                break;
            case QAbstractOAuth::Status::RefreshingToken:
                qDebug() << "Refreshing Token!";
                break;
            case QAbstractOAuth::Status::NotAuthenticated:
                qDebug() << "Not Authenticated!";
                break;
            case QAbstractOAuth::Status::TemporaryCredentialsReceived:
                qDebug() << "Temporary Credentials Received!";
                break;
            }
    });

    gCal.setScope("https://www.googleapis.com/auth/dialogflow");
    if (settings.value("refreshToken").toString().isEmpty())
    {
        gCal.grant();
    }
    else
    {
        qDebug() << "Refresh token: " + settings.value("refreshToken").toString();
        gCal.loadRefreshToken(settings.value("refreshToken").toByteArray());
    }

    a.setStyleSheet("QCompleter {QCompleter {background: #F0F0F0; nborder-radius: 10px; font-family: Roboto; font-style: normal; font-weight: normal; font-size: 12px; line-height: 14px; ncolor: #000000; }");
    a.setQuitOnLastWindowClosed(false);
    window.setCentralWidget(window.getLabel());
    window.show();

    return a.exec();
}
